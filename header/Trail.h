#include "TrailElement.h"

class Trail
{

public:
	Trail();
	void update(int x, int y, int radius);

	void draw();

private:
	const int DISTANCE = 4;
	static const int LENGTH = 15;
	TrailElement elements[LENGTH];
	int frames;
	int curIndex;
};