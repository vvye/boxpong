#include "Sprite.h"

#include "allegro5/color.h"

class TrailElement : public Sprite
{
private:
	float radius;

	ALLEGRO_COLOR color;

public:
	TrailElement();

	void update(double factor);

	void update();

	void draw();

	void set_radius(int radius)
	{
		TrailElement::radius = radius;
	}

	void set_color(ALLEGRO_COLOR color)
	{
		TrailElement::color = color;
	}
};