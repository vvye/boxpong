#include "allegro5/allegro.h"
#include "allegro5/display.h"
#include "allegro5/timer.h"
#include "allegro5/allegro_native_dialog.h"
#include "allegro5/allegro_primitives.h"

#include "Box.h"

class Game
{

public:

	Game();

	~Game();

	void run();

	void handle_update();

	void handle_redraw();

	void die(char const *text);

private:

	int width;
	int height;
	bool isRunning;
	bool redraw;

	ALLEGRO_DISPLAY *display;
	ALLEGRO_TIMER *timer;
	ALLEGRO_EVENT_QUEUE *eventQueue;

	Box box;
};