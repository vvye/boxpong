#pragma once

#include "allegro5/allegro_font.h"
#include "allegro5/allegro_ttf.h"

class Score
{
private:
	int score;
	ALLEGRO_FONT *font;

public:
	Score();

	~Score();

	void init();

	void increase_score();

	void draw();
};
