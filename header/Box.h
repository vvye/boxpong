#include "Sprite.h"
#include "Ball.h"
#include "Collectible.h"
#include "Score.h"

enum SIDE
{
	UP, RIGHT, DOWN, LEFT
};

class Box : public Sprite
{

public:

	Box();

	~Box();

	void init();

	void update();

	void draw();

	void set_dragged(bool isDragged)
	{
		Box::isDragged = isDragged;
	}

	void handle_drag(int dx, int dy);

	void set_game_width(int i);

	void set_game_height(int height);

private:
	int x;
	int y;
	int width;
	int height;
	int activeSideThickness;

	bool isDragged;
	SIDE activeSide;
	Ball ball;
	Collectible collectible;

	Score score;

	void handle_ball_collision();

	void handle_collectible_collision();

	void draw_side(SIDE side);

	void cycle_sides();

	int gameWidth;

	int gameHeight;

	void move_collectible();
};