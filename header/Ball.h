#include "Sprite.h"
#include "Trail.h"

class Ball : public Sprite
{

public:
	Ball();

	~Ball();

	int get_radius() const
	{
		return radius;
	}

	void reflect_h();

	void reflect_v();

	void update();

	void draw();

private:
	int radius;
	char xdir;
	char ydir;
	int speed;
	Trail trail;
};