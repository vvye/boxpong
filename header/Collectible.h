#pragma once

#include "Sprite.h"

class Collectible : public Sprite
{
public:
	Collectible();

	~Collectible();

	int get_radius() const
	{
		return radius;
	}

	void draw();

	void update();

private:
	int radius;
	float haloRadius;
};
