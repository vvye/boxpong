#pragma once

class Sprite
{

protected:
	int x;
	int y;

public:
	virtual void update() = 0;

	virtual void draw() = 0;

	int get_x() const
	{
		return x;
	}

	void set_x(int x)
	{
		Sprite::x = x;
	}

	int get_y() const
	{
		return y;
	}

	void set_y(int y)
	{
		Sprite::y = y;
	}
};