#include "Collectible.h"

#include "allegro5/allegro_primitives.h"


Collectible::Collectible()
{
	radius = 5;
	haloRadius = 5;
}

Collectible::~Collectible()
{
}

void Collectible::draw()
{
	al_draw_filled_circle(x, y, radius, al_map_rgb(255, 200, 0));
	al_draw_circle(x, y, haloRadius, al_map_rgb(155, 100, 0), 1);
}

void Collectible::update()
{
	haloRadius += 0.2;
	if (haloRadius >= 12)
	{
		haloRadius = 5;
	}
}
