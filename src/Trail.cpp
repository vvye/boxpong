#include "Trail.h"

Trail::Trail()
{
	frames = 0;
	curIndex = 0;

	TrailElement current;
	for (int i = 0; i < LENGTH; ++i)
	{
		current = elements[i];
		current.set_x(0);
		current.set_y(0);
		current.set_radius(0);
	}
}

void Trail::update(int x, int y, int radius)
{
	double factor;
	for (int i = 0; i < LENGTH; ++i)
	{
		factor = 1.0 / (LENGTH * DISTANCE);
		elements[i].update(factor);
	}

	frames = (frames + 1) % DISTANCE;
	if (frames == 0)
	{
		curIndex = (curIndex + 1) % LENGTH;
		elements[curIndex].set_x(x);
		elements[curIndex].set_y(y);
		elements[curIndex].set_radius(radius);
		elements[curIndex].set_color(al_map_rgb(255, 0, 0));
	}
}

void Trail::draw()
{
	for (int i = 0; i < LENGTH; ++i)
	{
		elements[i].draw();
	}
}