#include "allegro5/allegro_primitives.h"
#include "Score.h"

Score::Score()
{
	score = 0;
}


void Score::init()
{
	font = al_load_font("Munro.ttf", 48, 0);
}


Score::~Score()
{
	al_destroy_font(font);
}


void Score::increase_score()
{
	score++;
}


void Score::draw()
{
	al_draw_textf(font, al_map_rgb(150, 150, 150), 20, 20, ALLEGRO_ALIGN_LEFT, "%i", score);
}

