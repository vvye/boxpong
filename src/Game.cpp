#include "Game.h"


Game::Game()
{
	width = 800;
	height = 600;
	isRunning = true;
	redraw = true;

	if (!al_init())
	{
		die("Failed to initialize Allegro");
	}

	al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
	al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
	display = al_create_display(width, height);
	if (!display)
	{
		die("Failed to create display");
	}

	al_init_primitives_addon();
	al_install_mouse();

	eventQueue = al_create_event_queue();
	timer = al_create_timer(1.0 / 60);

	al_register_event_source(eventQueue, al_get_display_event_source(display));
	al_register_event_source(eventQueue, al_get_mouse_event_source());
	al_register_event_source(eventQueue, al_get_timer_event_source(timer));

	al_init_font_addon();
	al_init_ttf_addon();

	box.init();
	box.set_game_width(width);
	box.set_game_height(height);
}

Game::~Game()
{
	al_destroy_timer(timer);
	al_destroy_event_queue(eventQueue);
	al_destroy_display(display);
}

void Game::run()
{

	ALLEGRO_EVENT evt;

	al_start_timer(timer);
	while (isRunning)
	{
		al_wait_for_event(eventQueue, &evt);

		switch (evt.type)
		{
			case ALLEGRO_EVENT_DISPLAY_CLOSE:
				isRunning = false;
		        break;

			case ALLEGRO_EVENT_TIMER:
				redraw = true;
		        handle_update();
		        break;

			case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
				box.set_dragged(true);
		        break;

			case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
				box.set_dragged(false);
		        break;

			case ALLEGRO_EVENT_MOUSE_AXES:
				box.handle_drag(evt.mouse.dx, evt.mouse.dy);

			default:
				break;
		}

		handle_redraw();
	}
}

void Game::handle_redraw()
{
	if (redraw && al_is_event_queue_empty(eventQueue))
	{
		al_clear_to_color(al_map_rgb(0, 0, 0));
		box.draw();

		al_flip_display();
	}
}

void Game::die(char const *text)
{
	al_show_native_message_box(NULL, "Error", "", text, 0, ALLEGRO_MESSAGEBOX_ERROR);
	exit(-1);
}

void Game::handle_update()
{
	box.update();
}
