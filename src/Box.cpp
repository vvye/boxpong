#include "allegro5/allegro_primitives.h"
#include "Box.h"

Box::Box()
{
	x = 100;
	y = 100;
	width = 150;
	height = 150;
	activeSide = DOWN;
	activeSideThickness = 10;

	ball.set_x((x + width) / 2);
	ball.set_y((y + height) / 2 + 20);

	collectible.set_x(400);
	collectible.set_y(200);
}

Box::~Box()
{
}


void Box::init()
{
	score.init();
}

void Box::update()
{
	ball.update();
	collectible.update();
	handle_ball_collision();
	handle_collectible_collision();
}

void Box::draw()
{
	collectible.draw();

	draw_side(UP);
	draw_side(RIGHT);
	draw_side(DOWN);
	draw_side(LEFT);

	ball.draw();

	score.draw();
}

void Box::handle_ball_collision()
{
	int ballX = ball.get_x();
	int ballY = ball.get_y();
	int ballR = ball.get_radius();

	switch (activeSide)
	{
		case UP:
			if (ballX + ballR > x && ballX - ballR < x + width
			    && ballY + ballR > y - activeSideThickness && ballY - ballR < y)
			{
				ball.reflect_v();
				cycle_sides();
			}
	        break;
		case DOWN:
			if (ballX + ballR > x && ballX - ballR < x + width
			    && ballY + ballR > y + height && ballY - ballR < y + height + activeSideThickness)
			{
				ball.reflect_v();
				cycle_sides();
			}
	        break;
		case LEFT:
			if (ballY + ballR > y && ballY - ballR < y + height
			    && ballX + ballR > x - activeSideThickness && ballX - ballR < x)
			{
				ball.reflect_h();
				cycle_sides();
			}
	        break;
		case RIGHT:
			if (ballY + ballR > y && ballY - ballR < y + height
			    && ballX + ballR > x + width && ballX - ballR < x + width + activeSideThickness)
			{
				ball.reflect_h();
				cycle_sides();
			}
	        break;
	}
}

void Box::handle_collectible_collision()
{
	int ballX = ball.get_x();
	int ballY = ball.get_y();
	int ballR = ball.get_radius();

	int collX = collectible.get_x();
	int collY = collectible.get_y();
	int collR = collectible.get_radius();

	if (ballX + ballR > collX - collR
	    && ballX - ballR < collX + collR
	    && ballY + ballR > collY - collR
	    && ballY - ballR < collY + collR)
	{
		score.increase_score();
		move_collectible();
	}
}

void Box::move_collectible()
{
	int padding = 50;
	collectible.set_x(rand() % (gameWidth - padding) + padding / 2);
	collectible.set_y(rand() % (gameHeight - padding) + padding / 2);
}


void Box::handle_drag(int dx, int dy)
{
	if (isDragged)
	{
		x += dx;
		y += dy;
	}
}

void Box::draw_side(SIDE side)
{
	bool isActive = (side == activeSide);

	int thickness = isActive ? activeSideThickness : 1;
	ALLEGRO_COLOR color = isActive ? al_map_rgb(240, 50, 0) : al_map_rgb(128, 128, 128);
	float x1;
	float x2;
	float y1;
	float y2;

	switch (side)
	{
		case UP:
			x1 = x;
	        y1 = y - thickness;
	        x2 = x + width;
	        y2 = y;
	        break;
		case RIGHT:
			x1 = x + width;
	        y1 = y;
	        x2 = x + width + thickness;
	        y2 = y + height;
	        break;
		case DOWN:
			x1 = x;
	        y1 = y + height;
	        x2 = x + width;
	        y2 = y + height + thickness;
	        break;
		case LEFT:
		default:
			x1 = x - thickness;
	        y1 = y;
	        x2 = x;
	        y2 = y + height;
	        break;
	}

	al_draw_filled_rectangle(x1, y1, x2, y2, color);
}

void Box::cycle_sides()
{
	switch (activeSide)
	{
		case LEFT:
			activeSide = DOWN;
	        break;
		case DOWN:
			activeSide = RIGHT;
	        break;
		case RIGHT:
			activeSide = UP;
	        break;
		case UP:
			activeSide = LEFT;
	}
}

void Box::set_game_width(int width)
{
	gameWidth = width;
}


void Box::set_game_height(int height)
{
	gameHeight = height;
}
