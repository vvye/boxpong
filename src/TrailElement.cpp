#include "TrailElement.h"

#include "allegro5/allegro_primitives.h"

TrailElement::TrailElement()
{
	color = al_map_rgb(255, 255, 255);
}

void TrailElement::update(double factor)
{
	unsigned char r;
	unsigned char g;
	unsigned char b;
	al_unmap_rgb(color, &r, &g, &b);
	r = r - (unsigned char) (255 * factor);
	g = g - (unsigned char) (255 * factor);
	b = b - (unsigned char) (255 * factor);
	color = al_map_rgb(r, g, b);

	radius -= radius * factor;
	if (radius < 0)
	{
		radius = 0;
	}
}

void TrailElement::draw()
{
	al_draw_circle(x, y, radius, color, 1);
}

void TrailElement::update()
{
}
