#include "Ball.h"

#include <allegro5/allegro_primitives.h>

Ball::Ball()
{
	radius = 10;
	speed = 2;
	xdir = 1;
	ydir = 1;
}

Ball::~Ball()
{
}

void Ball::update()
{
	trail.update(x, y, radius);
	x += speed * xdir;
	y += speed * ydir;
}

void Ball::draw()
{
	trail.draw();
	al_draw_filled_circle(x, y, radius, al_map_rgb(240, 255, 255));
}

void Ball::reflect_h()
{
	xdir *= -1;
}

void Ball::reflect_v()
{
	ydir *= -1;
}
